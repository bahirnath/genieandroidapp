package Session;

import java.util.HashMap;

/**
 * Created by root on 5/1/17.
 */

public interface IOnClickOfSwitchChange {
    public void OnClickOfSwitchChange(HashMap<String,String> map);
    public void OnProgressChangeListener(HashMap<String,String> map);
}
